# Sistema - Mão na Massa
O projeto “Mão na Massa” será um sistema projetado para auxiliar artesões na gerência de seus artesanatos, exercendo funções como: controle dos produtos produzidos e dos produtos utilizados na produção (matéria-prima) através do cadastro e manutenção de estoque, controle dos produtos que entram e sai (vendas) e auxílio na precificação de produtos, visando a porcentagem de lucro desejada pelo cliente em cada produto.

A princípio o cliente trabalha com a produção de trufas caseiras de diversos sabores, portanto fazendo parte da categoria de produtos alimentícios dentro dos ramos do artesanato, porém a exigência foi um sistema com visão genérica, ou seja, que possa ser aplicado em outros tipos e ramos de artesanato, caso o cliente deseje mudar de ramo e produzir outros tipos de produtos. 

O cliente precisa de um sistema que o auxilie na gestão de informações durante as etapas da produção do artesanato, facilitando a manutenção dos dados salvos e o ajudando com organização de movimento dos produtos produzidos. 

O objetivo geral do projeto “Mão na Massa” é auxiliar durante todo o processo geral gestão de informações de produtos artesanais e caseiros.

Os objetivos específicos são descritos a seguir:

a)	Auxiliar no processo de precificação do produto;

b)	Controlar o estoque de matérias-primas e produtos;

c)	Auxiliar no processo de vendas;

d)	Calcular e exibir relatórios informativos.

Não faz parte do escopo do projeto transações monetárias e treinamento ao cliente.
